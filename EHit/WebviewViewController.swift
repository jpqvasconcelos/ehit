//
//  WebviewViewController.swift
//  EHit
//
//  Created by Joaquim Prince on 09/03/17.
//  Copyright © 2017 Deway. All rights reserved.
//

import UIKit

class WebviewViewController: ViewController, UIWebViewDelegate {
    
    @IBOutlet weak var webview: UIWebView!
    
    @IBOutlet weak var loadingIndicator: UIActivityIndicatorView!
    
    var url: URL! = URL(string: "https://instagram.com/ehitapp/")
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        webview.loadRequest(URLRequest(url: url))
        
    }
    
    func webViewDidStartLoad(_ webView: UIWebView) {
        
        loadingIndicator.startAnimating()
        
    }
    
    func webViewDidFinishLoad(_ webView: UIWebView) {
        
        loadingIndicator.stopAnimating()
        
        loadingIndicator.alpha = 0
        
    }
    
    func webView(_ webView: UIWebView, didFailLoadWithError error: Error) {
    
        loadingIndicator.stopAnimating()
        
        loadingIndicator.alpha = 0

    }
    
}
