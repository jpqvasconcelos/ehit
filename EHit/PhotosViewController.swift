//
//  PhotosViewController.swift
//  EHit
//
//  Created by Joaquim Prince on 21/03/17.
//  Copyright © 2017 Deway. All rights reserved.
//

import UIKit
import SKPhotoBrowser

class PhotosViewController: ViewController, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {

    @IBOutlet weak var photosCollectionView: UICollectionView!
    
    var photos: [RealmString] = []
    
    var photosBrowser: [SKPhoto] = []
    
    var browser: SKPhotoBrowser!
    
    let screen = UIScreen.main
    
    //MARK:
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        photosCollectionView.register(UINib(nibName: "PhotosCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "cell")
        
        createBrowserPhotos()


    }
    
    //MARK:

    public func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return photos.count
        
    }
    
    public func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = photosCollectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! PhotosCollectionViewCell
        
        let item = photos[(indexPath as NSIndexPath).row]
        
        cell.iconImageView.sd_setImage(with: URL(string: item.stringValue))
        
        return cell
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize(width: screen.bounds.width/3 - 8, height: 100)
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        browser.initializePageIndex(indexPath.row)
        
        present(browser, animated: true, completion: {})
        
    }
    
    //MARK:
    
    func createBrowserPhotos() {
        
        for each in photos {
            
            let photo = SKPhoto.photoWithImageURL(each.stringValue)
            
            photo.shouldCachePhotoURLImage = false
            
            photosBrowser.append(photo)
            
        }
        
        browser = SKPhotoBrowser(photos: photosBrowser)
        
    }

}
