//
//  PhotosService.swift
//  EHit
//
//  Created by Joaquim Prince on 21/03/17.
//  Copyright © 2017 Deway. All rights reserved.
//

import Foundation
import RealmSwift
import Alamofire
import AlamofireObjectMapper

protocol AgendaServiceDelegate {
    
    func postAgendaSuccessful(message: String?)
    
    func postAgendaFailed(data: Data?)
    
}

extension PromotionsServiceDelegate {
    
    func postAgendaSuccessful(message: String?) {
        
        fatalError("Este método deve ser implementado")
        
    }
    
    func postAgendaFailed(data: Data?) {
        
        fatalError("Este método deve ser implementado")
        
    }
    
}

class AgendaService {
    
    let delegate: AgendaServiceDelegate
    
    init(delegate: AgendaServiceDelegate) {
        
        self.delegate = delegate
    }
    
    func getEventos() {
        
        let url = baseURL + api + "/shows"
        
        Alamofire.request(url, method: .get).validate().responseArray { (response: DataResponse<[Evento]>) -> Void in
            
            switch response.result {
                
            case .success:
                
                if let data = response.result.value {
                    
                    Evento.deleteAll()
                    
                    Evento.saveAll(objects: data)
                    
                    self.delegate.postAgendaSuccessful(message: nil)
                    
                }
                
            case .failure:
                
                self.delegate.postAgendaFailed(data: nil)
                
            }
        }
    }
    
}
