//
//  Photo.swift
//  EHit
//
//  Created by Joaquim Prince on 17/03/17.
//  Copyright © 2017 Deway. All rights reserved.
//

import Foundation
import RealmSwift
import ObjectMapper

final class Promotion: Object, Mappable {
    
    dynamic var id = 0
    dynamic var title = ""
    dynamic var image = ""
    dynamic var url = ""
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    override static func primaryKey() -> String? {
        return "id"
    }
    
    func mapping(map: Map) {
        
        id <- map["id"]
        title <- map["title"]
        image <- map["image"]
        url <- map["url"]
        
    }
    
    static func save(object: Promotion) {
        
        let uiRealm = try! Realm()
        
        do {
            try uiRealm.write {
                uiRealm.add(object, update: true)
            }
        } catch  {
            print("Não foi possível persitir o objeto")
        }
    }
    
    static func saveAll(objects: [Promotion]) {
        
        let uiRealm = try! Realm()
        
        do {
            try uiRealm.write {
                uiRealm.add(objects, update: true)
            }
        } catch  {
            print("Não foi possível persitir os objetos")
        }
    }
    
    static func getByID(id: AnyObject) -> Promotion? {
        
        let uiRealm = try! Realm()
        
        let results = uiRealm.objects(Promotion.self).filter("id = %@", id)
        return results.first
    }
    
    static func all() -> [Promotion]? {
        
        let uiRealm = try! Realm()
        
        var list: [Promotion] = []
        list.append(contentsOf: uiRealm.objects(Promotion.self))
        return list
    }
    
    static func deleteObject(object: Promotion) {
        
        let uiRealm = try! Realm()
        
        do {
            try uiRealm.write {
                uiRealm.delete(object)
            }
        } catch  {
            print("Não foi possível persitir os objetos")
        }
    }
    
    static func deleteAll() {
        
        let objects: [Promotion] = all()!
        
        for object in objects {
            
            deleteObject(object: object)
        }
    }
    
}
