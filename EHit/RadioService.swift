//
//  RadioService.swift
//  EHit
//
//  Created by Joaquim Prince on 17/03/17.
//  Copyright © 2017 Deway. All rights reserved.
//

import Foundation
import RealmSwift
import Alamofire
import AlamofireObjectMapper

protocol RadioServiceDelegate {
    
    func postSuccessful(message: String?)
    
    func postFailed(data: Data?)
    
}

extension RadioServiceDelegate {
    
    func postSuccessful(message: String?) {
        
        fatalError("Este método deve ser implementado")
        
    }
    
    func postFailed(data: Data?) {
        
        fatalError("Este método deve ser implementado")
        
    }
    
}

class RadioService {
    
    let delegate: RadioServiceDelegate
    
    init(delegate: RadioServiceDelegate) {
        
        self.delegate = delegate
    }
    
    func getMusics() {
        
        let url = baseURL + "/music" + formatJson
        
        Alamofire.request(url, method: .get).validate().responseObject { (response: DataResponse<Radio>) -> Void in
            
            switch response.result {
                
            case .success:
                
                if let data = response.result.value {
                    
                    Radio.save(object: data)
                    
                    self.delegate.postSuccessful(message: nil)
                    
                }
                
            case .failure:

                self.delegate.postFailed(data: nil)
            
            }
            
        }
        
    }
    
}
