//
//  Photo.swift
//  EHit
//
//  Created by Joaquim Prince on 17/03/17.
//  Copyright © 2017 Deway. All rights reserved.
//

import Foundation
import RealmSwift
import ObjectMapper

final class Photo: Object, Mappable {
    
    dynamic var id = ""
    dynamic var name = ""
    dynamic var thumb = ""
    var images = List<RealmString>()

    required convenience init?(map: Map) {
        self.init()
    }
    
    override static func primaryKey() -> String? {
        return "id"
    }
    
    func mapping(map: Map) {
        
        id <- map["id"]
        name <- map["nome"]
        thumb <- map["thumb"]
        
        //images <- (map["imagems"], ListTransform<RealmString>())
        
        mappingImages(map: map["imagems"])
        
    }
    
    private func mappingImages(map: Map) {
        
        var data: [String] = []
        
        data <- map
        
        for each in data {
            
            let value = RealmString()
            
            value.stringValue = each
            
            images.append(value)
            
        }
                
    }
    
    static func save(object: Photo) {
        
        let uiRealm = try! Realm()
        
        do {
            try uiRealm.write {
                uiRealm.add(object, update: true)
            }
        } catch  {
            print("Não foi possível persitir o objeto")
        }
    }
    
    static func saveAll(objects: [Photo]) {
        
        let uiRealm = try! Realm()
        
        do {
            try uiRealm.write {
                uiRealm.add(objects, update: true)
            }
        } catch  {
            print("Não foi possível persitir os objetos")
        }
    }
    
    static func getByID(id: AnyObject) -> Photo? {
        
        let uiRealm = try! Realm()
        
        let results = uiRealm.objects(Photo.self).filter("id = %@", id)
        return results.first
    }
    
    static func all() -> [Photo]? {
        
        let uiRealm = try! Realm()
        
        var list: [Photo] = []
        list.append(contentsOf: uiRealm.objects(Photo.self))
        return list
    }
    
    static func deleteObject(object: Photo) {
        
        let uiRealm = try! Realm()
        
        do {
            try uiRealm.write {
                uiRealm.delete(object)
            }
        } catch  {
            print("Não foi possível persitir os objetos")
        }
    }
    
    static func deleteAll() {
        
        let objects: [Photo] = all()!
        
        for object in objects {
            
            deleteObject(object: object)
        }
    }
    
}

public struct ListTransform<T: RealmSwift.Object>: TransformType where T: Mappable {
    
    public init() { }
    
    public typealias Object = List<T>
    public typealias JSON = Array<Any>
    
    public func transformFromJSON(_ value: Any?) -> List<T>? {
        if let objects = Mapper<T>().mapArray(JSONObject: value) {
            let list = List<T>()
            list.append(objectsIn: objects)
            return list
        }
        return nil
    }
    
    public func transformToJSON(_ value: Object?) -> JSON? {
        return value?.flatMap { $0.toJSON() }
    }
    
}
