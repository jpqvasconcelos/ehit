//
//  PhotosService.swift
//  EHit
//
//  Created by Joaquim Prince on 21/03/17.
//  Copyright © 2017 Deway. All rights reserved.
//

import Foundation
import RealmSwift
import Alamofire
import AlamofireObjectMapper

protocol PromotionsServiceDelegate {
    
    func postPromotionsSuccessful(message: String?)
    
    func postPromotionsFailed(data: Data?)
    
}

extension PromotionsServiceDelegate {
    
    func postPromotionsSuccessful(message: String?) {
        
        fatalError("Este método deve ser implementado")
        
    }
    
    func postPromotionsFailed(data: Data?) {
        
        fatalError("Este método deve ser implementado")
        
    }
    
}

class PromotionsService {
    
    let delegate: PromotionsServiceDelegate
    
    init(delegate: PromotionsServiceDelegate) {
        
        self.delegate = delegate
    }
    
    func getPromotions() {
        
        let url = baseURL + api + "/promotions"
        
        Alamofire.request(url, method: .get).validate().responseArray { (response: DataResponse<[Promotion]>) -> Void in
            
            switch response.result {
                
            case .success:
                
                if let data = response.result.value {
                    
                    Promotion.deleteAll()
                    
                    Promotion.saveAll(objects: data)
                    
                    self.delegate.postPromotionsSuccessful(message: nil)
                    
                }
                
            case .failure:
                
                self.delegate.postPromotionsFailed(data: nil)
                
            }
        }
    }
    
}
