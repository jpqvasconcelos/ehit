//
//  PromotionsViewController.swift
//  EHit
//
//  Created by Joaquim Prince on 18/04/17.
//  Copyright © 2017 Deway. All rights reserved.
//

import UIKit

class PromotionsViewController: ViewController, UITableViewDataSource, UITableViewDelegate, PromotionsServiceDelegate, AgendaServiceDelegate {
    
    @IBOutlet weak var promotionsTableView: UITableView!
    
    //MARK:
    
    func imageTouched(sender: PrinceButton) {

        let url = promotions[sender.index.row].url

        if url != "" {
            
          UIApplication.shared.openURL(URL(string: url)!)
            
        }
        
    }
    
    //MARK:
    
    var promotions: [Promotion] = []
    
    var eventos: [Evento] = []
    
    var cellImageView: UIImageView!
    
    var isAgenda = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if isAgenda {
            
            eventos = (Evento.all()?.sorted(by: { $0.id < $1.id }))!
            
            AgendaService(delegate: self).getEventos()
            
        } else {
            
            promotions = (Promotion.all()?.sorted(by: { $0.id < $1.id }))!

            PromotionsService(delegate: self).getPromotions()
            
        }
        
        promotionsTableView.register(UINib(nibName: "PromotionsTableViewCell", bundle: nil), forCellReuseIdentifier: "cell")
        
        promotionsTableView.tableFooterView = UIView(frame: CGRect.zero)
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK:
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if isAgenda {
            
            return eventos.count
            
        }
        
        return promotions.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = promotionsTableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! PromotionsTableViewCell
        
        if isAgenda {
            
            let item = eventos[(indexPath as NSIndexPath).row]
            
            cell.iconImageView.sd_setImage(with: URL(string: item.image))
                        
            cell.selectionStyle = .none
                        
        } else {
            
            let item = promotions[(indexPath as NSIndexPath).row]
            
            cell.iconImageView.sd_setImage(with: URL(string: item.image))
            
            cell.imageButton.index = indexPath
            
            cell.imageButton.addTarget(self, action: #selector(imageTouched), for: .touchUpInside)
            
        }
        
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 192
        
    }
    
    //MARK:
    
    func postPromotionsSuccessful(message: String?) {
        
        promotions = (Promotion.all()?.sorted(by: { $0.id < $1.id }))!
        
        promotionsTableView.reloadData()
        
    }
    
    func postPromotionsFailed(data: Data?) {}
    
    func postAgendaSuccessful(message: String?) {
        
        eventos = (Evento.all()?.sorted(by: { $0.id < $1.id }))!
        
        promotionsTableView.reloadData()
        
    }
    
    func postAgendaFailed(data: Data?) {}

}
