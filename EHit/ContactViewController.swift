//
//  ContactViewController.swift
//  EHit
//
//  Created by Joaquim Prince on 09/03/17.
//  Copyright © 2017 Deway. All rights reserved.
//

import UIKit
import AKNumericFormatter

class ContactViewController: ViewController, UITextViewDelegate, ContactServiceDelegate, UIGestureRecognizerDelegate {

    @IBOutlet weak var nameTextField: UITextField!
    
    @IBOutlet weak var emailTextField: UITextField!
    
    @IBOutlet weak var phoneTextField: UITextField!
    
    @IBOutlet weak var messageTextView: UITextView!
    
    @IBOutlet weak var heightToMoveUpViews: NSLayoutConstraint!
    
    @IBAction func tapGestureTouched(_ sender: Any) {
        
        self.view.endEditing(true)
        
    }
    
    @IBAction func sendTouched(_ sender: Any) {
        
        if nameTextField.text != "", let name = nameTextField.text,
            emailTextField.text != "", let email = emailTextField.text,
            phoneTextField.text != "", let phone = phoneTextField.text,
            messageTextView.text != "", let message = messageTextView.text {
            
            ContactService(delegate: self).postContact(name: name, email: email, phone: phone, description: message)
            
        } else {
            
            showAlertWith(title: "Todos os campos são obrigatórios", message: "Preencha todos os campos antes de enviar seu contato.")
            
        }
        
    }
    
    //MARK:
    
    let character: String = "*"
    
    let placeholderColor = UIColor(red: 127/255.0, green: 127/255.0, blue: 127/255.0, alpha: 1)
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        nameTextField.setLeftPaddingPoints(8)
        
        emailTextField.setLeftPaddingPoints(8)
        
        phoneTextField.setLeftPaddingPoints(8)
        
        phoneTextField.numericFormatter = AKNumericFormatter(mask: "(**) *****-****", placeholderCharacter: character.utf16.first!)
        
        messageTextView.delegate = self
            
    }
    
    //MARK:
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        
        if (textView.text == " Insira sua mensagem aqui...") {
            
            textView.text = ""
            
            textView.textColor = .white
            
        }
        
        heightToMoveUpViews.constant = 256
        
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        
        if (textView.text == "") {
            
            textView.text = " Insira sua mensagem aqui..."
            
            textView.textColor = placeholderColor
            
        }
        
        heightToMoveUpViews.constant = 0
        
    }
    
    //MARK:
    
    public func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool {
        
        if (touch.view == self.view) {
            
            return true
            
        }
        
        return false
        
    }
    
    //MARK:
    
    func cleanFields() {
        
        nameTextField.text = ""
        
        emailTextField.text = ""
        
        phoneTextField.text = ""
        
        messageTextView.text = " Insira sua mensagem aqui..."
        
        messageTextView.textColor = placeholderColor
        
    }
    
    //MARK:
    
    func postSuccessful(message: String?) {
        
        showAlertWith(title: "Mensagem enviada", message: "Sua mensagem foi enviada com sucesso. Obrigado pelo contato")
        
        cleanFields()
        
    }
    
    func postFailed(data: Data?) {
        
        showAlertWith(title: "Mensagem enviada", message: "Sua mensagem foi enviada com sucesso. Obrigado pelo contato")
        
        cleanFields()
        
    }

}
