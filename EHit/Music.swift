//
//  Music.swift
//  EHit
//
//  Created by Joaquim Prince on 17/03/17.
//  Copyright © 2017 Deway. All rights reserved.
//

import Foundation
import RealmSwift
import ObjectMapper

final class Music: Object, Mappable {
    
    dynamic var id = 0
    dynamic var name = ""
    dynamic var url = ""
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    override static func primaryKey() -> String? {
        return "id"
    }
    
    func mapping(map: Map) {
        id <- map["id"]
        name <- map["nome"]
        url <- map["url"]
    }
    
    static func save(object: Music) {
        
        let uiRealm = try! Realm()
        
        do {
            try uiRealm.write {
                uiRealm.add(object, update: true)
            }
        } catch  {
            print("Não foi possível persitir o objeto")
        }
    }
    
    static func saveAll(objects: [Music]) {
        
        let uiRealm = try! Realm()
        
        do {
            try uiRealm.write {
                uiRealm.add(objects, update: true)
            }
        } catch  {
            print("Não foi possível persitir os objetos")
        }
    }
    
    static func getByID(id: AnyObject) -> Music? {
        
        let uiRealm = try! Realm()
        
        let results = uiRealm.objects(Music.self).filter("id = %@", id)
        return results.first
    }
    
    static func all() -> [Music]? {
        
        let uiRealm = try! Realm()
        
        var list: [Music] = []
        list.append(contentsOf: uiRealm.objects(Music.self))
        return list
    }
    
    static func deleteObject(object: Music) {
        
        let uiRealm = try! Realm()
        
        do {
            try uiRealm.write {
                uiRealm.delete(object)
            }
        } catch  {
            print("Não foi possível persitir os objetos")
        }
    }
    
    static func deleteAll() {
        
        let objects: [Music] = all()!
        
        for object in objects {
            
            deleteObject(object: object)
        }
    }
    
}
