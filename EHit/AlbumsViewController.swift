//
//  AlbumsViewController.swift
//  EHit
//
//  Created by Joaquim Prince on 21/03/17.
//  Copyright © 2017 Deway. All rights reserved.
//

import UIKit

class AlbumsViewController: ViewController, UITableViewDataSource, UITableViewDelegate , PhotosServiceDelegate {
    
    @IBOutlet weak var albumsTableView: UITableView!
    
    var albums: [Photo] = []

    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        albums = (Photo.all()?.sorted(by: {$0.id < $1.id }))!
        
        PhotostService(delegate: self).getPhotos()
        
        albumsTableView.register(UINib(nibName: "NewsTableViewCell", bundle: nil), forCellReuseIdentifier: "cell")
        
        albumsTableView.tableFooterView = UIView(frame: CGRect.zero)

    }
    
    //MARK:
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return albums.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = albumsTableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! NewsTableViewCell
        
        let item = albums[(indexPath as NSIndexPath).row]
        
        cell.iconImageView.sd_setImage(with: URL(string: item.thumb))
        
        let count = item.images.count
        
        if count == 0 || count == 1 {
            
           cell.subtitleLabel.text = "\(count) foto"
            
        } else {
            
            cell.subtitleLabel.text = "\(count) fotos"
            
        }
        
        cell.titleLabel.text = item.name
        
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 80
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let selectedAlbum = Array(albums[(indexPath as NSIndexPath).row].images)
        
        let photosStoryboard = UIStoryboard(name: "Photos", bundle: nil)
        
        let photosNavigationController = photosStoryboard.instantiateInitialViewController()
        
        let photosVC = photosNavigationController?.childViewControllers[0] as! PhotosViewController
        
        photosVC.photos = selectedAlbum
        
        slideMenuController()?.changeMainViewController(photosNavigationController!, close: true)
        
    }

    //MARK:
    
    func postSuccessful(message: String?) {
        
        albums = (Photo.all()?.sorted(by: {$0.id < $1.id }))!
        
        albumsTableView.reloadData()
        
    }
    
    func postFailed(data: Data?) {
    }

}
