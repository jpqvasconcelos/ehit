//
//  HighlightService.swift
//  EHit
//
//  Created by Joaquim Prince on 20/03/17.
//  Copyright © 2017 Deway. All rights reserved.
//

import Foundation
import RealmSwift
import Alamofire
import AlamofireObjectMapper

protocol HighlightServiceDelegate {
    
    func postHighlightSuccessful(message: String?)
    
    func postHighlightFailed(data: Data?)
    
}

extension HighlightServiceDelegate {
    
    func postHighlightSuccessful(message: String?) {
        
        fatalError("Este método deve ser implementado")
        
    }
    
    func postHighlightFailed(data: Data?) {
        
        fatalError("Este método deve ser implementado")
        
    }
    
}

class HightlightService {
    
    let delegate: HighlightServiceDelegate
    
    init(delegate: HighlightServiceDelegate) {
        
        self.delegate = delegate
    }
    
    func getHightlights() {
        
        let url = baseURL + "/highlights" + formatJson
        
        Alamofire.request(url, method: .get).validate().responseArray { (response: DataResponse<[Hightlight]>) -> Void in
            
            switch response.result {
                
            case .success:
                
                if let data = response.result.value {
                    
                    Hightlight.deleteAll()
                    
                    Hightlight.saveAll(objects: data)
                    
                    self.delegate.postHighlightSuccessful(message: nil)
                }
                
            case .failure:
                
                self.delegate.postHighlightFailed(data: nil)
                
                
            }
            
        }
        
    }
    
}
