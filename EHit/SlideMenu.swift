//
//  SlideMenu.swift
//  EHit
//
//  Created by Joaquim Prince on 08/03/17.
//  Copyright © 2017 Deway. All rights reserved.
//

import UIKit
import SlideMenuControllerSwift

class SlideMenu: SlideMenuController {
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
    }
    
    override func awakeFromNib() {
        
        let homeVC = UIStoryboard(name: "Radio", bundle: nil).instantiateInitialViewController()
        
        let sidebarVC = UIStoryboard(name: "Sidebar", bundle: nil).instantiateInitialViewController()
        
        mainViewController = homeVC
        
        leftViewController = sidebarVC
        
        super.awakeFromNib()
        
    }
    
}
