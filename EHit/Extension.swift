//
//  Extension.swift
//  EHit
//
//  Created by Joaquim Prince on 08/03/17.
//  Copyright © 2017 Deway. All rights reserved.
//

import Foundation
import KDEAudioPlayer
import REPagedScrollView

extension AudioPlayer {
    
    func isPlaying() -> Bool {
        
        switch self.state {
            
        case .playing:
            
            return true
            
        default:
            
            return false
        }
        
    }
    
}

extension TimeInterval {
    
    func toMinutes() -> String {
        
        let minutes = Int(self) / 60 % 60
        
        let seconds = Int(self) % 60
        
        return String(format:"%02i:%02i", minutes, seconds)
        
    }
    
}

extension UIImage {
    
    func imageWithColor(color: UIColor) -> UIImage? {
        
        var image = withRenderingMode(.alwaysTemplate)
        
        UIGraphicsBeginImageContextWithOptions(size, false, scale)
        
        color.set()
        
        image.draw(in: CGRect(x: 0, y: 0, width: size.width, height: size.height))
        
        image = UIGraphicsGetImageFromCurrentImageContext()!
        
        UIGraphicsEndImageContext()
        
        return image
    }
    
    func scaleImage(toSize newSize: CGSize) -> UIImage? {
        
        let newRect = CGRect(x: 0, y: 0, width: newSize.width, height: newSize.height).integral
        
        UIGraphicsBeginImageContextWithOptions(newSize, false, 0)
        
        if let context = UIGraphicsGetCurrentContext() {
            
            context.interpolationQuality = .high
            
            let flipVertical = CGAffineTransform(a: 1, b: 0, c: 0, d: -1, tx: 0, ty: newSize.height)
            
            context.concatenate(flipVertical)
            
            context.draw(self.cgImage!, in: newRect)
            
            let newImage = UIImage(cgImage: context.makeImage()!)
            
            UIGraphicsEndImageContext()
            
            return newImage
        }
        
        return nil
    }
    
}

extension REPagedScrollView {
    
    func isMax() -> Bool {

        if self.pageControl.currentPage+1 == self.pages.count {
            
            return true
            
        }
        
        return false
        
    }
    
}

extension UITextField {
    
    func setLeftPaddingPoints(_ amount:CGFloat) {
        
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: amount, height: self.frame.size.height))
        
        self.leftView = paddingView
        
        self.leftViewMode = .always
        
    }
    
    func setRightPaddingPoints(_ amount:CGFloat) {
        
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: amount, height: self.frame.size.height))
        
        self.rightView = paddingView
        
        self.rightViewMode = .always
        
    }
    
}
