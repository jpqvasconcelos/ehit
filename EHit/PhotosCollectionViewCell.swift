//
//  PhotosCollectionViewCell.swift
//  EHit
//
//  Created by Joaquim Prince on 21/03/17.
//  Copyright © 2017 Deway. All rights reserved.
//

import UIKit

class PhotosCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var iconImageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
