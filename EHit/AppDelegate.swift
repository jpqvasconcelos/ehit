//
//  AppDelegate.swift
//  EHit
//
//  Created by Joaquim Prince on 08/03/17.
//  Copyright © 2017 Deway. All rights reserved.
//

import UIKit
import RealmSwift
import Firebase

var uiRealm = try! Realm()

//var baseURL = "http://starmusic-ws.ideliveryapp.com.br" //TEST
var baseURL = "http://starmusic.deway.com.br" //PRODUCAO

var api = "/api/v2/artists/24"

var formatJson = "/24/?format=json"

var formatPlist = "/24/?format=plist"

var idService = "/24"

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        
        application.beginReceivingRemoteControlEvents()
        
        UIApplication.shared.statusBarStyle = .lightContent //application.isStatusBarHidden = true //
        
        registerForPushNotification()
        
        FIRApp.configure()
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.tokenRefreshNotification), name: .firInstanceIDTokenRefresh, object: nil)
        
        return true
        
    }
    
    func applicationWillResignActive(_ application: UIApplication) {}
    
    func applicationDidEnterBackground(_ application: UIApplication) {
        
        //FIRMessaging.messaging().disconnect()
        
    }
    
    func applicationWillEnterForeground(_ application: UIApplication) {}
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        
        connectToFcm()
        
    }
    
    func applicationWillTerminate(_ application: UIApplication) {
    }
    
    //MARK:
    
    func registerForPushNotification() {
        
        let notificationSettings = UIUserNotificationSettings(types: [.badge, .alert, .sound], categories: nil)
        
        UIApplication.shared.registerUserNotificationSettings(notificationSettings)
        
    }
    
    func application(_ application: UIApplication, didRegister notificationSettings: UIUserNotificationSettings) {
        
        if notificationSettings.types != .none {
            
            application.registerForRemoteNotifications()
            
        }
        
    }
    
    //MARK:
    
    func tokenRefreshNotification(notification: NSNotification) {
        
        if let refreshedToken = FIRInstanceID.instanceID().token() {
            
            //print("InstanceID token: \(refreshedToken)")
            
            NotificationService(delegate: self).getNotifications(token: refreshedToken)
            
        }
        
        connectToFcm()
        
    }
    
    func connectToFcm() {
        
        guard FIRInstanceID.instanceID().token() != nil else {
            return
        }
        

        FIRMessaging.messaging().disconnect()
        
        FIRMessaging.messaging().connect { (error) in
            
            if error != nil {
                
                //print("Unable to connect with FCM. \(error?.localizedDescription ?? "")")
                
            } else {
                
                //print("Connected to FCM.")
            }
        }
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any]) {
        
        // If you are receiving a notification message while your app is in the background,
        // this callback will not be fired till the user taps on the notification launching the application.
        // TODO: Handle data of notification
        
        // Print message ID.
        //print("Message ID: \(userInfo["gcm.message_id"]!)")
        
        // Print full message.
        //print("%@", userInfo)
        
    }

}

extension AppDelegate: NotificationServiceDelegate {
    
    func postNotificationSuccesful() {}
    
    func postNotificationFailed() {}
    
}
