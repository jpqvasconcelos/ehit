//
//  NotificationService.swift
//  EHit
//
//  Created by Deway on 02/05/17.
//  Copyright © 2017 Deway. All rights reserved.
//

import Foundation
import Alamofire

protocol NotificationServiceDelegate {
    
    func postNotificationSuccesful()
    
    func postNotificationFailed()
    
}

extension NotificationServiceDelegate {
    
    func postNotificationSuccesful() {
        
        fatalError("Metodo deverá ser implementado")
    
    }
    
    func postNotificationFailed() {
        
        fatalError("Metodo deverá ser implementado")
        
    }
    
}

class NotificationService {
    
    let delegate: NotificationServiceDelegate
    
    init(delegate: NotificationServiceDelegate) {
        
        self.delegate = delegate
        
    }
    
    func getNotifications(token: String) {
        
        let url = baseURL + "/notifications/add-iphone-user/24/?token=FCM-" + token

        Alamofire.request(url, method: .get).validate().responsePropertyList { response in
            
            switch response.result {
                
            case .success:
                
                if let data = response.result.value as? [String: String] {
                 
                if !data.isEmpty {
                    
                    if data.first?.value == "success" {
                        
                        self.delegate.postNotificationSuccesful()
                        
                    } else {
                        
                        self.delegate.postNotificationFailed()
                        
                    }
                    
                }
                
                }
                
            case .failure:
                
            if let error = response.result.error {
                
                self.delegate.postNotificationFailed()
                
                }
                
            }
            
        }
        
    }
    
}
