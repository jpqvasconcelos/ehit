//
//  PromotionsTableViewCell.swift
//  EHit
//
//  Created by Deway on 19/04/17.
//  Copyright © 2017 Deway. All rights reserved.
//

import UIKit

class PromotionsTableViewCell: UITableViewCell {
    
    
    @IBOutlet weak var iconImageView: UIImageView!
    
    @IBOutlet weak var imageButton: PrinceButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
