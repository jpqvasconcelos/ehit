
//
//  Header.swift
//  EHit
//
//  Created by Joaquim Prince on 09/03/17.
//  Copyright © 2017 Deway. All rights reserved.
//

import UIKit
import REPagedScrollView

class Header: UITableViewCell {

    @IBOutlet weak var hightlightsView: UIView!
    
    @IBOutlet weak var statusImageView: UIImageView!
    
    @IBOutlet weak var statusButton: UIButton!
    
    @IBOutlet weak var offlineImageView: UIImageView!
    var isON: Bool = true
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
