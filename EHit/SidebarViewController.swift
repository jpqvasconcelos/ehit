//
//  SidebarViewController.swift
//  EHit
//
//  Created by Joaquim Prince on 08/03/17.
//  Copyright © 2017 Deway. All rights reserved.
//

import UIKit

class SidebarViewController: ViewController, UITableViewDataSource, UITableViewDelegate {

    @IBOutlet weak var sidebarTableView: UITableView!
    
    @IBOutlet weak var logoImageView: UIImageView!
    
    var selectedCell: SidebarTableViewCell!
    
    let items: [String] = ["Separator", "Música", "Promoções", "Fotos", "Agenda", "Notícias", "Separator", "Siga nosso Instagram", "Separator", "Contato"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        sidebarTableView.register(UINib(nibName: "SidebarTableViewCell", bundle: nil), forCellReuseIdentifier: "cell")
        
        sidebarTableView.register(UINib(nibName: "SeparatorTableViewCell", bundle: nil), forCellReuseIdentifier: "separator")
        
        sidebarTableView.tableFooterView = UIView(frame: CGRect.zero)

    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return items.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let item = items[(indexPath as NSIndexPath).row]
        
        if item == "Separator" {
            
            let cell = sidebarTableView.dequeueReusableCell(withIdentifier: "separator", for: indexPath) as! SeparatorTableViewCell
            
            return cell
            
        } else {
           
            let cell = sidebarTableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! SidebarTableViewCell
            
            if indexPath.row == 1 && slideMenuController()?.mainViewController?.childViewControllers.first is RadioViewController {
                
                //cell.leftView.alpha = 1
                
                //cell.titleLabel.textColor = redThemeColor
                
                //selectedCell = cell
                
            }
            
            cell.titleLabel.text = item
            
            return cell
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        let item = items[(indexPath as NSIndexPath).row]
        
        if item == "Separator" {
            
            return 24
            
        }
        
        return 40
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        setSelectedCellStyle(indexPath)
        
        switch items[indexPath.row] {
            
        case "Contato":
            
            let contactStoryboard = UIStoryboard(name: "Contact", bundle: nil)
            
            let contactNavigationController = contactStoryboard.instantiateInitialViewController()
            
            slideMenuController()?.changeMainViewController(contactNavigationController!, close: true)
            
        case "Promoções":
            
            let promotionsStoryboard = UIStoryboard(name: "Promotions", bundle: nil)
            
            let promotionsNavigationController = promotionsStoryboard.instantiateInitialViewController()
            
            slideMenuController()?.changeMainViewController(promotionsNavigationController!, close: true)
            
            break
            
        case "Agenda":
            
            let promotionsStoryboard = UIStoryboard(name: "Promotions", bundle: nil)
            
            let webviewNavigationController = promotionsStoryboard.instantiateInitialViewController()
            
            let promotionsVC = webviewNavigationController?.childViewControllers[0] as! PromotionsViewController
            
            promotionsVC.isAgenda = true
            
            slideMenuController()?.changeMainViewController(webviewNavigationController!, close: true)
            
            break
            
        case "Notícias":
            
            let newsStoryboard = UIStoryboard(name: "News", bundle: nil)
            
            let newsNavigationController = newsStoryboard.instantiateInitialViewController()
            
            slideMenuController()?.changeMainViewController(newsNavigationController!, close: true)
            
        case "Siga nosso Instagram":
            
            let webviewStoryboard = UIStoryboard(name: "Webview", bundle: nil)
            
            let webviewNavigationController = webviewStoryboard.instantiateInitialViewController()
            
            slideMenuController()?.changeMainViewController(webviewNavigationController!, close: true)
            
            //showLoading()
            
        case "Fotos":
            
            let albumsStoryboard = UIStoryboard(name: "Albums", bundle: nil)
            
            let albumsNavigationController = albumsStoryboard.instantiateInitialViewController()
            
            slideMenuController()?.changeMainViewController(albumsNavigationController!, close: true)
            
        default:
            
            let radioStoryboard = UIStoryboard(name: "Radio", bundle: nil)
            
            let radioNavigationController = radioStoryboard.instantiateInitialViewController()
            
            slideMenuController()?.changeMainViewController(radioNavigationController!, close: true)
            
        }
        
        sidebarTableView.deselectRow(at: indexPath, animated: true)
        
    }
    
    func setSelectedCellStyle(_ indexPath: IndexPath) {
        
//        let cell = sidebarTableView.cellForRow(at: indexPath) as! SidebarTableViewCell
//        
//        selectedCell.leftView.alpha = 0
//        
//        selectedCell.titleLabel.textColor = UIColor.white
//        
//        cell.leftView.alpha = 1
//        
//        cell.titleLabel.textColor = redThemeColor
//        
//        selectedCell = cell
        
    }

}
