//
//  MessageResponse.swift
//  EHit
//
//  Created by Joaquim Prince on 04/04/17.
//  Copyright © 2017 Deway. All rights reserved.
//

import Foundation
import ObjectMapper

final class MessageResponse: Mappable {
    
    dynamic var status = 0
    dynamic var success = ""
    dynamic var message = ""
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        
        status <- map["status"]
        success <- map["success"]
        message <- map["message"]
        message <- map["Erro"]
    }
}

