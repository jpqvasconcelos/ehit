//
//  ViewController.swift
//  EHit
//
//  Created by Joaquim Prince on 08/03/17.
//  Copyright © 2017 Deway. All rights reserved.
//

import UIKit
import SlideMenuControllerSwift
import SwiftSpinner

class ViewController: UIViewController {
    
    let redColor = UIColor(red: 181/255.0, green: 36/255.0, blue: 34/255.0, alpha: 1)
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        navigationController?.navigationBar.barTintColor = redColor
        
        addNavigationItemLeftButton()
        
        addNavigationItemRightButton()
        
        addNavigationItemLogoImage()
        
    }
    
    func addNavigationItemLeftButton() {
        
        let img = UIImage(named: "menu")?.scaleImage(toSize: CGSize(width: 14, height: 14))
        
        let sidebarButtonItem = UIBarButtonItem(image: img, style: UIBarButtonItemStyle.plain, target: self, action: #selector(sidebarTocuhed))
        
        sidebarButtonItem.tintColor = UIColor.white
        
        self.navigationItem.setLeftBarButton(sidebarButtonItem, animated: true)
        
    }
    
    func addNavigationItemRightButton() {
        
        let blankButtonItem = UIBarButtonItem(customView: UIView(frame: CGRect(x: 0, y: 0, width: 24, height: 24)))
        
        self.navigationItem.setRightBarButton(blankButtonItem, animated: true)
        
    }
    
    func addNavigationItemLogoImage() {
        
        let logoImageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 32, height: 32))

        logoImageView.image = UIImage(named: "logo_nav")?.imageWithColor(color: UIColor.white)
        
        logoImageView.contentMode = .scaleAspectFit
        
        self.navigationItem.titleView = logoImageView
        
    }
    
    func sidebarTocuhed() {
        
        slideMenuController()?.openLeft()
        
    }
    
    func showLoading() {
        
        SwiftSpinner.show("Carregando...")
    }
    
    func hideLoading() {
        
        SwiftSpinner.hide()
    }
    
    func showAlertWith(title: String, message: String) {
        
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
        
        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
        
        self.present(alert, animated: true, completion: nil)
        
    }
    
}
