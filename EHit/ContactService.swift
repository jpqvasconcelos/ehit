//
//  ContactService.swift
//  EHit
//
//  Created by Joaquim Prince on 21/03/17.
//  Copyright © 2017 Deway. All rights reserved.
//

import Foundation
import RealmSwift
import Alamofire
import AlamofireObjectMapper

protocol ContactServiceDelegate {
    
    func postSuccessful(message: String?)
    
    func postFailed(data: Data?)
    
}

extension ContactServiceDelegate {
    
    func postSuccessful(message: String?) {
        
        fatalError("Este método deve ser implementado")
        
    }
    
    func postFailed(data: Data?) {
        
        fatalError("Este método deve ser implementado")
        
    }
    
}

class ContactService {
    
    let delegate: ContactServiceDelegate
    
    init(delegate: ContactServiceDelegate) {
        
        self.delegate = delegate
    }
    
    func postContact(name: String, email: String, phone: String, description: String) {
        
        let url = baseURL + "/contact" + formatPlist
        
        let parameters: [String: AnyObject] = ["name": name as AnyObject, "email": email as AnyObject, "phone": phone as AnyObject, "description": description as AnyObject]
        
        Alamofire.request(url, method: .post, parameters: parameters).validate().responseObject { (response: DataResponse<MessageResponse>) -> Void in
            
            switch response.result {
                
            case .success:
                
                self.delegate.postSuccessful(message: nil)
                
            case .failure:
                
                self.delegate.postFailed(data: nil)
                
            }
        }
    }
    
}
