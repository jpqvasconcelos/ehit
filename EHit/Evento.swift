//
//  Photo.swift
//  EHit
//
//  Created by Joaquim Prince on 17/03/17.
//  Copyright © 2017 Deway. All rights reserved.
//

import Foundation
import RealmSwift
import ObjectMapper

final class Evento: Object, Mappable {
    
    dynamic var id = 0
    dynamic var image = ""
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    override static func primaryKey() -> String? {
        return "id"
    }
    
    func mapping(map: Map) {
        
        id <- map["id"]
        image <- map["image"]
        
    }
    
    static func save(object: Evento) {
        
        let uiRealm = try! Realm()
        
        do {
            try uiRealm.write {
                uiRealm.add(object, update: true)
            }
        } catch  {
            print("Não foi possível persitir o objeto")
        }
    }
    
    static func saveAll(objects: [Evento]) {
        
        let uiRealm = try! Realm()
        
        do {
            try uiRealm.write {
                uiRealm.add(objects, update: true)
            }
        } catch  {
            print("Não foi possível persitir os objetos")
        }
    }
    
    static func getByID(id: AnyObject) -> Evento? {
        
        let uiRealm = try! Realm()
        
        let results = uiRealm.objects(Evento.self).filter("id = %@", id)
        return results.first
    }
    
    static func all() -> [Evento]? {
        
        let uiRealm = try! Realm()
        
        var list: [Evento] = []
        list.append(contentsOf: uiRealm.objects(Evento.self))
        return list
    }
    
    static func deleteObject(object: Evento) {
        
        let uiRealm = try! Realm()
        
        do {
            try uiRealm.write {
                uiRealm.delete(object)
            }
        } catch  {
            print("Não foi possível persitir os objetos")
        }
    }
    
    static func deleteAll() {
        
        let objects: [Evento] = all()!
        
        for object in objects {
            
            deleteObject(object: object)
        }
    }
    
}
