//
//  PhotosService.swift
//  EHit
//
//  Created by Joaquim Prince on 21/03/17.
//  Copyright © 2017 Deway. All rights reserved.
//

import Foundation
import RealmSwift
import Alamofire
import AlamofireObjectMapper

protocol NewsServiceDelegate {
    
    func postSuccessful(message: String?)
    
    func postFailed(data: Data?)
    
}

extension NewsServiceDelegate {
    
    func postSuccessful(message: String?) {
        
        fatalError("Este método deve ser implementado")
        
    }
    
    func postFailed(data: Data?) {
        
        fatalError("Este método deve ser implementado")
        
    }
    
}

class NewsService {
    
    let delegate: NewsServiceDelegate
    
    init(delegate: NewsServiceDelegate) {
        
        self.delegate = delegate
    }
    
    func getNews() {
        
        let url = baseURL + "/news" + formatJson
        
        Alamofire.request(url, method: .get).validate().responseObject { (response: DataResponse<KeyNews>) -> Void in
            
            switch response.result {
                
            case .success:
                
                if let data = response.result.value {
                    
                    KeyNews.deleteAll()
                    
                    KeyNews.save(object: data)
                    
                    self.delegate.postSuccessful(message: nil)
                }
                
            case .failure:
                
                self.delegate.postFailed(data: nil)
                
                
            }
            
        }


    }
    
}
