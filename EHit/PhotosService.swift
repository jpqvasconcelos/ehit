//
//  PhotosService.swift
//  EHit
//
//  Created by Joaquim Prince on 21/03/17.
//  Copyright © 2017 Deway. All rights reserved.
//

import Foundation
import RealmSwift
import Alamofire
import AlamofireObjectMapper

protocol PhotosServiceDelegate {
    
    func postSuccessful(message: String?)
    
    func postFailed(data: Data?)
    
}

extension PhotosServiceDelegate {
    
    func postSuccessful(message: String?) {
        
        fatalError("Este método deve ser implementado")
        
    }
    
    func postFailed(data: Data?) {
        
        fatalError("Este método deve ser implementado")
        
    }
    
}

class PhotostService {
    
    let delegate: PhotosServiceDelegate
    
    init(delegate: PhotosServiceDelegate) {
        
        self.delegate = delegate
    }
    
    func getPhotos() {
        
        let url = baseURL + "/photos" + formatJson
                
        Alamofire.request(url, method: .get).validate().responseArray { (response: DataResponse<[Photo]>) -> Void in
            
            switch response.result {
                
            case .success:
                
                if let data = response.result.value {
                    
                    Photo.deleteAll()
                    
                    Photo.saveAll(objects: data)
                    
                    self.delegate.postSuccessful(message: nil)

                }
                
            case .failure:
                
                self.delegate.postFailed(data: nil)
                
            }
        }
    }
    
}
