//
//  RadioViewController.swift
//  EHit
//
//  Created by Joaquim Prince on 08/03/17.
//  Copyright © 2017 Deway. All rights reserved.
//

import UIKit
import KDEAudioPlayer
import SDWebImage
import REPagedScrollView
import AVFoundation

class RadioViewController: ViewController, UITableViewDataSource, UITableViewDelegate, RadioServiceDelegate, HighlightServiceDelegate, AudioPlayerDelegate {

    @IBOutlet weak var radioTableView: UITableView!
    
    @IBOutlet weak var playButton: UIButton!
    
    @IBOutlet weak var previousButton: UIButton!
    
    @IBOutlet weak var nextButton: UIButton!
    
    @IBOutlet weak var timeSlider: UISlider!
    
    @IBOutlet weak var musicLabel: UILabel!
    
    @IBOutlet weak var startDurationLabel: UILabel!
    
    @IBOutlet weak var endDurationLabel: UILabel!
    
    @IBOutlet weak var loadingIndicator: UIActivityIndicatorView!
    
    @IBOutlet weak var loadingIndicatorCenterXConstraint: NSLayoutConstraint!
    
    
    @IBOutlet weak var blockSliderView: UIView!
    
    //MARK:
    //MARK: Player Buttons
    
    @IBAction func previousTouched(_ sender: Any) {
                
        if audioPlayer.hasPrevious {
            
            if audioPlayer.currentItem != liveRadioItem {
                
                if let index = audioPlayer.currentItemIndexInQueue {
                    
                    if index > 0 {
                        
                       audioPlayer.play(items: audioItens, startAtIndex: audioPlayer.currentItemIndexInQueue! - 1)
                        
                    }
                    
                }
                
            }
            
        }
        
    }
    
    @IBAction func nextTouched(_ sender: Any) {
        
        if audioPlayer.hasNext {
            
            if audioPlayer.currentItem != liveRadioItem {
                
                if let index = audioPlayer.currentItemIndexInQueue {
                    
                    if index < audioItens.count-1 {

                        audioPlayer.play(items: audioItens, startAtIndex: audioPlayer.currentItemIndexInQueue! + 1)

                    }
                    
                }
                
            }
            
        }
        
    }
    
    @IBAction func playTouched(_ sender: Any) {
        
        if musicLabel.text != "" {
            
            if audioPlayer.isPlaying() {
                
                audioPlayer.pause()
                
                updatePlayButton()
                
            } else {
                
                audioPlayer.resume()
                
                updatePlayButton()
                
            }
            
        }
        
    }
    
    //MARK:
    //MARK: Slider Buttons
    
    @IBAction func sliderTouchDown(_ sender: UISlider) {
        
        audioPlayer.pause()

    }
    
    
    @IBAction func sliderTouchUpInside(_ sender: UISlider) {
        
        audioPlayer.resume()

        if let item = audioPlayer.currentItemSeekableRange {
            
            let maxRange = item.latest
            
            let pickedValue = sender.value
            
            if pickedValue <= Float(maxRange) {
                
                audioPlayer.seek(to: TimeInterval(pickedValue))
                
            } else {
                
                let rangeWithMargin = maxRange.subtracting(5)
                
                audioPlayer.seek(to: rangeWithMargin)
                
                timeSlider.value = Float(rangeWithMargin)
                
            }
            
        }
        
    }
    

    @IBAction func sliderValueChanged(_ sender: UISlider) {
        
        let timeInterval = TimeInterval(exactly: sender.value)
        
        startDurationLabel.text = timeInterval?.toMinutes()
        
    }
    
    //MARK:
    
    func turnRadioTouched() {
        
        if hightlightsHeader.isON {
            
            audioPlayer.pause()
            
            hightlightsHeader.isON = false
            
        } else {
            
            audioPlayer.play(item: liveRadioItem)
            
            hightlightsHeader.isON = true
            
        }
        
        blockSliderView.alpha = 1
        
        endDurationLabel.text = "00:00"
        
        updateRadioButton()
        
    }
    
    //MARK:

    var audioPlayer: AudioPlayer = AudioPlayer()
    
    var audioItens: [AudioItem] = []
    
    var audioPlayerDelegate : AudioPlayerDelegate!
    
    var musics: [Music] = []
    
    var hightlights: [Hightlight] = []
    
    var liveRadio: Music!
    
    var liveRadioItem: AudioItem!
    
    var hightlightsHeader: Header!
    
    var pagedScrollView: REPagedScrollView!
    
    var hightlightsView: UIView!
    
    var timer: Timer!
    
    var isLoading: Bool = false
    
    var localFilesURL: [URL] = []
    
    let screen = UIScreen.main.bounds
    
    let documentDirectoryURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
    
    //MARK:
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        musics = (Music.all())!
        
        hightlights = (Hightlight.all())!
        
        setupRadio()
        
        RadioService(delegate: self).getMusics()
        
        HightlightService(delegate: self).getHightlights()
        
        audioPlayerDelegate = self
        
        audioPlayer.delegate = audioPlayerDelegate

        radioTableView.register(UINib(nibName: "RadioTableViewCell", bundle: nil), forCellReuseIdentifier: "cell")
        
        radioTableView.register(UINib(nibName: "Header", bundle: nil), forHeaderFooterViewReuseIdentifier: "cell")
        
        hightlightsHeader = Bundle.main.loadNibNamed("Header", owner: nil, options: nil)?.first as! Header
        
        hightlightsView = hightlightsHeader.hightlightsView
        
        let height = screen.height/2 - hightlightsHeader.statusImageView.bounds.height
        
        pagedScrollView = REPagedScrollView(frame: CGRect(x: 0, y: 0, width: screen.width, height: height))
        
        pagedScrollView.delegate = self
        
        timeSlider.setThumbImage(UIImage(named: "dot")?.imageWithColor(color: UIColor.white), for: .normal)
        
        radioTableView.tableFooterView = UIView(frame: CGRect.zero)
        
        if liveRadioItem != nil {
            
            audioPlayer.play(item: liveRadioItem)
            
        }
        //configureView()
    }
    
    var player = AVPlayer()
    
    func configureView() {
        
        let url = "http://streaming27.hstbr.net:8188/live"
        let playerItem = AVPlayerItem(url: URL(string: url)!)
        player = AVPlayer(playerItem:playerItem)
        player.rate = 1.0;
        player.play()
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        self.becomeFirstResponder()
        
    }
    
    override var canBecomeFirstResponder: Bool {
        
        return true
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        
        timer.invalidate()
        
        audioPlayer.stop()
        
    }
    
    //MARK:

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return musics.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = radioTableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! RadioTableViewCell
        
        let item = musics[(indexPath as NSIndexPath).row]
        
        cell.titleLabel.text = item.name
        
        cell.counterLabel.text = "\(indexPath.row + 1)"
        
        let itemURL = URL(string: item.url)
        
        cell.downloadButton.index = indexPath
        
        cell.downloadButton.addTarget(self, action: #selector(downloadAudio), for: .touchUpInside)
        
        for each in localFilesURL {
            
            if each.lastPathComponent == itemURL?.lastPathComponent {
                
                setDownloadButtonToGreen(cell: cell)
                
                cell.downloadButton.removeTarget(self, action: #selector(downloadAudio), for: .touchUpInside)
                
            }
            
        }
        
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let selectedMusic = musics[(indexPath as NSIndexPath).row]
        
        if audioItens.count > 0 {
            
            for index in 0...audioItens.count-1 {
                
                if audioItens[index].title == selectedMusic.name {
                    
                    hightlightsHeader.isON = false
                    
                    updateRadioButton()
                    
                    audioPlayer.play(items: audioItens, startAtIndex: index)
                    
                }
                
            }
            
        }
        
        radioTableView.deselectRow(at: indexPath, animated: true)
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 32
        
    }

    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        return screen.height/2
        
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        hightlightsHeader.statusButton.addTarget(self, action: #selector(turnRadioTouched), for: .touchUpInside)
        
        return hightlightsHeader
        
    }
    
    //MARK:
    
    func setupRadio() {
        
        if musics.count > 0 {
            
            liveRadio = musics.removeFirst()
            
            liveRadioItem = AudioItem(mediumQualitySoundURL: URL(string: liveRadio.url))!
            
            liveRadioItem.title = liveRadio.name
            
            if !audioPlayer.isPlaying() {
                
                audioPlayer.play(item: liveRadioItem)
                
            }
            
        }
        
    }
    
    func createPlaylist() {
        
        audioItens.removeAll()

        for each in musics {
            
            if each.url != "" {
                
                if let serviceURL = URL(string: each.url) {
                    
                    var item = AudioItem(mediumQualitySoundURL: serviceURL)!
                    
                    for localURL in localFilesURL {
                        
                        if localURL.lastPathComponent == serviceURL.lastPathComponent {
                            
                            item = AudioItem(highQualitySoundURL: localURL)!
                            
                        }
                        
                    }
                    
                    item.title = each.name
                    
                    audioItens.append(item)
                }
                
            }

        }
        
    }
    
    //MARK:
    
    func updateRadioButton() {
        
        let statusImage = (hightlightsHeader.isON ? UIImage(named: "turn_on3") : UIImage(named: "turn_off2"))
        
        hightlightsHeader.statusImageView.image = statusImage
        
    }
    
    func updatePlayButton() {
        
        let playImage = (audioPlayer.isPlaying() ? UIImage(named: "pause_slice4") : UIImage(named: "play_slice4"))
        
        playButton.setImage(playImage, for: UIControlState())
        
    }
    
    func updateHightlights() {
        
        let indexToPage: Int = pagedScrollView.pageControl.currentPage + 1
        
        if pagedScrollView.isMax() {
            
            pagedScrollView.scrollToPage(with: 0, animated: true)
            
        } else {
            
            pagedScrollView.scrollToPage(with: UInt(indexToPage), animated: true)
            
        }
        
    }
    
    //MARK:
        
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        
        timer.invalidate()
        
        timer = Timer.scheduledTimer(timeInterval: 5, target: self, selector: #selector(updateHightlights), userInfo: nil, repeats: true)
        
    }
    
    //MARK:
    
    func showLoadingIndicator() {
        
        isLoading = true
        
        loadingIndicatorCenterXConstraint.constant = 0

        playButton.alpha = 0
        
        loadingIndicator.alpha = 1
        
        loadingIndicator.startAnimating()

    }
    
    func hideLoadingIndicator() {
        
        isLoading = false
        
        loadingIndicatorCenterXConstraint.constant = -4

        playButton.alpha = 1
        
        loadingIndicator.alpha = 0
        
        if audioPlayer.currentItem != liveRadioItem {
            
            blockSliderView.alpha = 0
            
        }
        
        loadingIndicator.stopAnimating()
        
    }
    
    func showLoadingInCell(_ cell: RadioTableViewCell) {
        
        cell.downloadButton.alpha = 0
        
        cell.loadingIndicator.alpha = 1
        
        cell.loadingIndicator.startAnimating()
        
    }
    
    func hideLoadingInCell(_ cell: RadioTableViewCell) {
        
        cell.downloadButton.alpha = 1
        
        cell.loadingIndicator.alpha = 0
        
        cell.loadingIndicator.stopAnimating()
        
    }
    
    //MARK:
    
    func audioPlayer(_ audioPlayer: AudioPlayer, didChangeStateFrom from: AudioPlayerState, to state: AudioPlayerState) {

        switch state {
            
        default: updatePlayButton()
            
        }
        
    }
    
    func audioPlayer(_ audioPlayer: AudioPlayer, willStartPlaying item: AudioItem) {

        musicLabel.text = item.title
        
        startDurationLabel.text = "00:00"

        timeSlider.setValue(0, animated: true)
        
        showLoadingIndicator()
        
    }
    
    func audioPlayer(_ audioPlayer: AudioPlayer, didUpdateProgressionTo time: TimeInterval, percentageRead: Float) {

        if audioPlayer.currentItem != liveRadioItem {
            
            startDurationLabel.text = time.toMinutes()
            
            timeSlider.value += 0.5
            
        }
        
        if percentageRead >= 0.0 {
            
            hideLoadingIndicator()
            
        }

    }

    func audioPlayer(_ audioPlayer: AudioPlayer, didFindDuration duration: TimeInterval, for item: AudioItem) {

        timeSlider.setValue(0, animated: true)
        
        if audioPlayer.currentItem != liveRadioItem {
            
            endDurationLabel.text = duration.toMinutes()
            
            timeSlider.maximumValue = Float(duration)
            
        }

    }
    
    //MARK:
    
    func postSuccessful(message: String?) {
        
        musics = (Music.all())!
        
        setupRadio()
        
        getLocalFilesUrl()
        
        createPlaylist()
        
        radioTableView.reloadData()

    }
    
    func postFailed(data: Data?) {
    
        getLocalFilesUrl()
        
        createPlaylist()
        
        radioTableView.reloadData()
    
    }
    
    func postHighlightSuccessful(message: String?) {

        hightlights = (Hightlight.all())!
        
        for each in hightlights {
            
            let imgView = UIImageView(frame: CGRect(x: 0, y: 0, width: (pagedScrollView.bounds.width), height: (pagedScrollView.bounds.height - 32)))
            
            imgView.sd_setImage(with: URL(string: each.imagem))
            
            pagedScrollView.addPage(imgView)
            
        }
        
        hightlightsView?.addSubview(pagedScrollView)
        
        timer = Timer.scheduledTimer(timeInterval: 5, target: self, selector: #selector(updateHightlights), userInfo: nil, repeats: true)
        
    }
    
    func postHighlightFailed(data: Data?) {
        
        hightlightsHeader.offlineImageView.alpha = 1
    
        timer = Timer.scheduledTimer(timeInterval: 5, target: self, selector: #selector(updateHightlights), userInfo: nil, repeats: true)
    
    }

    //MARK:
    
    func downloadAudio(sender: PrinceButton) {
        
        if audioItens.count > 0 {
            
            let cell = self.radioTableView.cellForRow(at: sender.index) as! RadioTableViewCell
            
            showLoadingInCell(cell)
            
            let indexPath = sender.index.row
            
            let audioURL = audioItens[indexPath].mediumQualityURL
            
            let destinationURL = documentDirectoryURL.appendingPathComponent(audioURL.url.lastPathComponent)
            
            if FileManager.default.fileExists(atPath: destinationURL.path) {
                
                //print("The file already exists at path")
                
                hideLoadingInCell(cell)
                
            } else {
                
                URLSession.shared.downloadTask(with: audioURL.url, completionHandler: { (location, response, error) -> Void in
                    
                    guard let location = location, error == nil else { return }
                    
                    do {
                        
                        try FileManager.default.moveItem(at: location, to: destinationURL)
                        
                        //print("File moved to documents folder")
                        
                        self.hideLoadingInCell(cell)
                        
                        self.getLocalFilesUrl()
                        
                        DispatchQueue.main.async() {
                            
                            self.setDownloadButtonToGreen(cell: cell)
                            
                            self.createPlaylist()
                            
                        }
                        
                    } catch let error as NSError {
                        
                        //print(error.localizedDescription)
                        
                    }
                    
                }).resume()
                
            }
            
        }
        
    }
    
    func getLocalFilesUrl() {
        
        do {
            
            let directoryContents = try FileManager.default.contentsOfDirectory(at: documentDirectoryURL, includingPropertiesForKeys: nil, options: [])

            localFilesURL = directoryContents.filter{ $0.pathExtension == "mp3" }
            
        } catch let error as NSError {
            
            //print(error.localizedDescription)
            
        }
        
    }
    
    func setDownloadButtonToGreen(cell: RadioTableViewCell) {
        
        cell.downloadButton.tintColor = UIColor(red: 0/255.0, green: 241/255.0, blue: 76/255.0, alpha: 1)
        
    }

    //MARK:
    
    override func remoteControlReceived(with event: UIEvent?) {
        
        if let event = event {
            
            audioPlayer.remoteControlReceived(with: event)
            
        }
    }
    
}
