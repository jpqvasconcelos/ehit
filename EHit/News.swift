//
//  News.swift
//  EHit
//
//  Created by Joaquim Prince on 17/03/17.
//  Copyright © 2017 Deway. All rights reserved.
//

import Foundation
import RealmSwift
import ObjectMapper

final class News: Object, Mappable {
    
    dynamic var key = ""
    dynamic var day = ""
    dynamic var month = ""
    dynamic var title = ""
    dynamic var descri = ""
    dynamic var thumb = ""
    dynamic var image = ""
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    override static func primaryKey() -> String? {
        return "key"
    }
    
    func mapping(map: Map) {
        
        key <- map["key"]
        day <- map["dia"]
        month <- map["mes"]
        title <- map["title"]
        descri <- map["content"]
        thumb <- map["thumb"]
        image <- map["image"]
        
    }
    
    static func save(object: News) {
        
        let uiRealm = try! Realm()
        
        do {
            try uiRealm.write {
                uiRealm.add(object, update: true)
            }
        } catch  {
            print("Não foi possível persitir o objeto")
        }
    }
    
    static func saveAll(objects: [News]) {
        
        let uiRealm = try! Realm()
        
        do {
            try uiRealm.write {
                uiRealm.add(objects, update: true)
            }
        } catch  {
            print("Não foi possível persitir os objetos")
        }
    }
    
    static func getByID(id: AnyObject) -> News? {
        
        let uiRealm = try! Realm()
        
        let results = uiRealm.objects(News.self).filter("id = %@", id)
        return results.first
    }
    
    static func all() -> [News]? {
        
        let uiRealm = try! Realm()
        
        var list: [News] = []
        list.append(contentsOf: uiRealm.objects(News.self))
        return list
    }
    
    static func deleteObject(object: News) {
        
        let uiRealm = try! Realm()
        
        do {
            try uiRealm.write {
                uiRealm.delete(object)
            }
        } catch  {
            print("Não foi possível persitir os objetos")
        }
    }
    
    static func deleteAll() {
        
        let objects: [News] = all()!
        
        for object in objects {
            
            deleteObject(object: object)
        }
    }
    
}

