//
//  NewsViewController.swift
//  EHit
//
//  Created by Deway on 10/04/17.
//  Copyright © 2017 Deway. All rights reserved.
//

import UIKit

class NewsViewController: ViewController, NewsServiceDelegate, UITableViewDataSource, UITableViewDelegate {

    @IBOutlet weak var newsTableView: UITableView!
    
    var news: [News] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        NewsService(delegate: self).getNews()
        
        let keyNews = KeyNews.all()
        
        if let data = keyNews?.first {
            
            news = Array(data.news)
            
        }

        newsTableView.register(UINib(nibName: "NewsTableViewCell", bundle: nil), forCellReuseIdentifier: "cell")
        
        newsTableView.tableFooterView = UIView(frame: CGRect.zero)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return news.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = newsTableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! NewsTableViewCell
        
        let item = news[(indexPath as NSIndexPath).row]
        
        cell.titleLabel.text = item.title
        
        cell.subtitleLabel.text = item.month
        
        cell.iconImageView.sd_setImage(with: URL(string: item.thumb))
        
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 92
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let newsDetailStoryboard = UIStoryboard(name: "NewsDetail", bundle: nil)
        
        let newsDetailNavigationController = newsDetailStoryboard.instantiateInitialViewController()
        
        let newsDetailVC = newsDetailNavigationController?.childViewControllers[0] as! NewsDetailViewController
        
        newsDetailVC.new = news[(indexPath as NSIndexPath).row]
        
        slideMenuController()?.changeMainViewController(newsDetailNavigationController!, close: true)
        
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    overNewsTride func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    func postSuccessful(message: String?) {
        
        let keyNews = KeyNews.all()
        
        if let data = keyNews?.first {
            
            news = Array(data.news)
            
        }
        
        newsTableView.reloadData()
    }
    
    func postFailed(data: Data?) {}

}
