//
//  NewsDetailViewController.swift
//  EHit
//
//  Created by Deway on 10/04/17.
//  Copyright © 2017 Deway. All rights reserved.
//

import UIKit

class NewsDetailViewController: ViewController {

    @IBOutlet weak var iconImageView: UIImageView!
    
    
    @IBOutlet weak var titleLabel: UILabel!
    
    
    @IBOutlet weak var subtitleLabel: UILabel!
    
    @IBOutlet weak var descriptionTextView: UITextView!
    
    
    var new: News!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        iconImageView.sd_setImage(with: URL(string: new.image))
        
        titleLabel.text = new.title
        
        subtitleLabel.text = new.month
        
        descriptionTextView.text = new.descri
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
