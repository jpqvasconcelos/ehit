//
//  Radio.swift
//  EHit
//
//  Created by Joaquim Prince on 17/03/17.
//  Copyright © 2017 Deway. All rights reserved.
//

import Foundation
import RealmSwift
import ObjectMapper

final class Radio: Object, Mappable {
    
    dynamic var id = 0
    var musics = List<Music>()
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    override static func primaryKey() -> String? {
        return "id"
    }
    
    func mapping(map: Map) {
        
        mappingMusics(map: map["musicas"])
        
    }
    
    private func mappingMusics(map: Map) {

        var data: [Music] = []
        
        data <- map
        
        musics.append(contentsOf: data)
        
    }
    
    static func save(object: Radio) {
        
        let uiRealm = try! Realm()
        
        do {
            try uiRealm.write {
                uiRealm.add(object, update: true)
            }
        } catch  {
            print("Não foi possível persitir o objeto")
        }
    }
    
    static func saveAll(objects: [Radio]) {
        
        let uiRealm = try! Realm()
        
        do {
            try uiRealm.write {
                uiRealm.add(objects, update: true)
            }
        } catch  {
            print("Não foi possível persitir os objetos")
        }
    }
    
    static func all() -> [Radio]? {
        
        let uiRealm = try! Realm()
        
        var list: [Radio] = []
        
        list.append(contentsOf: uiRealm.objects(Radio.self))
        
        return list
        
    }
    
    static func deleteObject(object: Radio) {
        
        let uiRealm = try! Realm()
        
        do {
            
            try uiRealm.write {
                
                uiRealm.delete(object)
                
            }
            
        } catch  {
            print("Não foi possível persitir os objetos")
        }
    }
    
    static func deleteAll() {
        
        let objects: [Radio] = all()!
        
        for object in objects {
            
            deleteObject(object: object)
            
        }
    }
    
}

