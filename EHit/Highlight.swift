//
//  Highlight.swift
//  EHit
//
//  Created by Joaquim Prince on 20/03/17.
//  Copyright © 2017 Deway. All rights reserved.
//

import Foundation
import RealmSwift
import ObjectMapper

final class Hightlight: Object, Mappable {
    
    dynamic var key = ""
    dynamic var publication = ""
    dynamic var title = ""
    dynamic var link = ""
    dynamic var imagem = ""
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    override static func primaryKey() -> String? {
        return "key"
    }
    
    func mapping(map: Map) {
        
        key <- map["key"]
        publication <- map["publication"]
        title <- map["title"]
        link <- map["link"]
        imagem <- map["imagem"]
        
    }
    
    static func save(object: Hightlight) {
        
        let uiRealm = try! Realm()
        
        do {
            try uiRealm.write {
                uiRealm.add(object, update: true)
            }
        } catch  {
            print("Não foi possível persitir o objeto")
        }
    }
    
    static func saveAll(objects: [Hightlight]) {
        
        let uiRealm = try! Realm()
        
        do {
            try uiRealm.write {
                uiRealm.add(objects, update: true)
            }
        } catch  {
            print("Não foi possível persitir os objetos")
        }
    }
    
    static func all() -> [Hightlight]? {
        
        let uiRealm = try! Realm()
        
        var list: [Hightlight] = []
        
        list.append(contentsOf: uiRealm.objects(Hightlight.self))
        
        return list
        
    }
    
    static func deleteAll() {
        
        let objects: [Hightlight] = all()!
        
        for object in objects {
            
            deleteObject(object: object)
        }
    }
    
    static func deleteObject(object: Hightlight) {
        
        let uiRealm = try! Realm()
        
        do {
            try uiRealm.write {
                uiRealm.delete(object)
            }
        } catch  {
            print("Não foi possível persitir os objetos")
        }
    }
    
}
