//
//  News.swift
//  EHit
//
//  Created by Joaquim Prince on 17/03/17.
//  Copyright © 2017 Deway. All rights reserved.
//

import Foundation
import RealmSwift
import ObjectMapper

final class KeyNews: Object, Mappable {
    
    dynamic var id = 0
    var news = List<News>()
    //var keys = List<Keys>()
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    override static func primaryKey() -> String? {
        return "id"
    }
    
    func mapping(map: Map) {
        
        mappingNews(map: map["news"])
        
    }
    
    private func mappingNews(map: Map) {
        
        var data: [News] = []
        
        data <- map
        
        news.append(contentsOf: data)
        
    }
    
    static func save(object: KeyNews) {
        
        let uiRealm = try! Realm()
        
        do {
            try uiRealm.write {
                uiRealm.add(object, update: true)
            }
        } catch  {
            print("Não foi possível persitir o objeto")
        }
    }
    
    static func saveAll(objects: [KeyNews]) {
        
        let uiRealm = try! Realm()
        
        do {
            try uiRealm.write {
                uiRealm.add(objects, update: true)
            }
        } catch  {
            print("Não foi possível persitir os objetos")
        }
    }
    
    static func getByID(id: AnyObject) -> KeyNews? {
        
        let uiRealm = try! Realm()
        
        let results = uiRealm.objects(KeyNews.self).filter("id = %@", id)
        return results.first
    }
    
    static func all() -> [KeyNews]? {
        
        let uiRealm = try! Realm()
        
        var list: [KeyNews] = []
        list.append(contentsOf: uiRealm.objects(KeyNews.self))
        return list
    }
    
    static func deleteObject(object: KeyNews) {
        
        let uiRealm = try! Realm()
        
        do {
            try uiRealm.write {
                uiRealm.delete(object)
            }
        } catch  {
            print("Não foi possível persitir os objetos")
        }
    }
    
    static func deleteAll() {
        
        let objects: [KeyNews] = all()!
        
        for object in objects {
            
            deleteObject(object: object)
        }
    }
    
}

